import React from 'react';



class Addtext extends React.Component {

state = {
  com :[],
  txt : ""}



  onChangeinput = (event) =>{
    this.setState({txt : event.target.value})
  };

  onSubmitenter = (event) =>{
    event.preventDefault();
    console.log(this.state.txt);
    this.setState({com: [...this.state.com, this.state.txt] , txt: ""})
  };


  removeItem = (index)=> {
      let com = [...this.state.com];
      com = com.filter((element,indexEL)=>indexEL!==index);
      this.setState({com})
  };


render(){
  return(
    <div>
      <form onSubmit={this.onSubmitenter}>
        <input value={this.state.txt} onChange={this.onChangeinput} />
        <button className="btnadd" disabled={!this.state.txt}>Add</button>
      </form>

    <div>
    <ul>
      {this.state.com.map((todo,index) => (
        <li  key={index}>
          {todo}
          <button onClick={()=>this.removeItem(index)}>delet</button>
        </li>
      ))}
      </ul>
    </div>

    </div>
  )
}

}


export default Addtext;
